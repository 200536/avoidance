﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    public GameObject EnemyPrefab;
    float span = 0;
    float delta = 0;

    // Update is called once per frame
    void Update()
    {
        span = Random.Range(-0.1f, 100.0f);
        delta += Time.deltaTime;
        if (delta > span)//この時間の感覚で読み込む
        {
            delta = 0;
            GameObject go = Instantiate(EnemyPrefab) as GameObject;
            int px = Random.Range(-4, 4);
            go.transform.position = new Vector3(10, px, 0);
        }

    }
}
