﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointGeneraor : MonoBehaviour
{
    public GameObject PointPrefab;
    float span = 0;
    float delta = 0;
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {

        span = Random.Range(-1.0f, 100.0f);
        delta = 0;
        delta += Time.deltaTime;
        if (delta > span)//この時間の感覚で読み込む
        {
            this.delta = 0;
            GameObject go = Instantiate(PointPrefab) as GameObject;
            int px = Random.Range(-4, 4);
            go.transform.position = new Vector3(10, px, 0);
        }
    }
}
